# VMware [Tanzu CE](https://tanzucommunityedition.io/) - Ubuntu

You can use the [vRealize Automation on-premises](https://blogs.vmware.com/management/2021/02/announcing-vmware-vrealize-automation-8-3.html)'s "Tanzu-CE-bootstrap" [Cloud Template](https://blogs.vmware.com/management/2020/08/vmware-cloud-templates.html) with embedded cloud-init + "input parameters", for deploying a [Tanzu CE](https://tanzucommunityedition.io/) cluster at AWS VMs, this way you remove the complexity of the pre-requisites installation, settings configurations and automate the actual [Tanzu CE](https://tanzucommunityedition.io/) cluster's creation at AWS, furthermore, you can include specific custom user selections at request time for different type of [Tanzu CE](https://tanzucommunityedition.io/) installations.

IMPORTANT, this effort is targeted for lab testing, education and it is not Production ready, however it can be modified to meet the production criteria.

**REQUIREMENTS:**

This [Cloud Template](https://blogs.vmware.com/management/2020/08/vmware-cloud-templates.html) has been tested with the following releases:

- Image: Ubuntu 18.04.1 LTS with Cloud-Init enabled (AWS ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20190722.1).
- vRAC or vRA 8.3 or higher (You need to have it configured and ready for deploying Cloud Templates).
- Minimum Flavor Recommendations: AWS m5.large or equivalent

**INPUTS DETAILS:**

  environment:<br>
    type: string <br>
    Description: Replace the default capability tag with available in your Cloud Environment, e.g.  cloud:aws <br>

  region:<br>
    type: string <br>
    default: us-east-1 <br>
    description: Available AWS Regions in your Cloud Zone <br>

  clusterName: <br>
    type: string <br>
    default: wwko <br>
    description: Name your Tanzu Cluster <br>
    
  sshKeyName: <br>
    type: string <br>
    description: The name of the SSH private key that it will be created for deploying Tanzu CE with your AWS account. <br>
    title: SSH Key Name <br>

  workerMachine: <br>
    type: number <br>
    default: 2 <br>
    description: Number of Workers Servers in your Tanzu Cluster <br>
    minimum: 2 <br>
    maximum: 4 <br>

  clusterPlan: <br>
    type: string <br>
    default: dev <br>
    description: Select your Tanzu Cluster Plan <br>

  controlPlanType: <br>
    type: string <br>
    default: m5.large <br>
    description: The Amazon EC2 instance type to use for cluster control plane nodes, for example t3.small or m5.large <br>

  nodeMachineType: <br>
    type: string <br>
    default: m5.large <br>
    description: The Amazon EC2, instance type to use for cluster worker nodes, for example t3.small or m5.large <br>

  identityType: <br>
    type: string <br>
    default: none <br>
    description: Identity Management Infrastructure Type <br>
    title: Identity Management Type <br>

  sshKeyAccess: <br>
    type: string <br>
    description: The content of the SSH public key for accessing your VM. <br>
    title: SSH Pub Key Content <br>

**INTERNAL SECRET VARIABLES:**

- Define your AWS_ACCESS_KEY_ID as secret named ${secret.wwko-aakid}
- Define your AWS_SECRET_ACCESS_KEY as secret named ${secret.wwko-asak}
<br>
    [Cloud Assembly’s Single Secret Store.](https://blogs.vmware.com/management/2021/03/single-secret-store.html)
<br><br>

**TROUBLESHOOTING:**

You can modify most of the input attributes and add new properties, also remember to update the "write_files" section

**RELEASE:**

Version: 1.0<br>
Updated: Ap4 4 2022




